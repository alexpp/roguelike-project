package com.example.alexander.roguelikeandroid.output;

import android.content.Context;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.alexander.roguelikeandroid.game.GameHandler;

public class MySurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private DrawThread drawThread;
    public GameHandler gameHandler;
    public int width, height;
    public CallBack listener;

    public MySurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        getHolder().addCallback(this);
    }



    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height) {	
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        width = holder.getSurfaceFrame().width();
        height = holder.getSurfaceFrame().height();
        gameHandler = new GameHandler(this);
        listener.onCreateComplete();
        drawThread = new DrawThread(getHolder(), getResources(), this.gameHandler);
        drawThread.setRunning(true);
        drawThread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        boolean retry = true;
        // завершаем работу потока
        drawThread.setRunning(false);
        while (retry) {
            try {
                drawThread.join();
                retry = false;
            } catch (InterruptedException e) {
                // если не получилось, то будем пытаться еще и еще
            }
        }
    }
    public interface CallBack {
        void onCreateComplete();
    }
}