package com.example.alexander.roguelikeandroid.objects;

/**
 * Created by Alexander on 10.11.2016.
 */

public abstract class BaseAliveObject extends BaseObject {

    public int health;
    public int armor;
    public int strength;

    public BaseAliveObject(int x, int y, char charModel, String name, int color, int health, int armor, int strength) {
        super(x, y, charModel, name, color);
        this.health = health;
        this.armor = armor;
        this.strength = strength;
    }
}
