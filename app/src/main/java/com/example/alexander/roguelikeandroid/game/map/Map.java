package com.example.alexander.roguelikeandroid.game.map;

/**
 * Created by Alexander on 23.11.2016.
 */

public class Map {

    public static final int FREE = 0;
    public static final int BLOCKED = 1;
    public static final int PLAYER = 2;
    public static final int MONSTER = 3;

    public int width;
    public int height;
    public int[][] map;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        map = new int[height][width];
    }

    public void clear() {
        if (map != null) {
            for (int[] ints : map) {
                for (int i = 0; i < ints.length; i++) {
                    ints[i] = FREE;
                }
            }
        }
    }

    public void wallPerimeter() {
        for (int i = 0; i < width; i++) {
            map[height-1][i] = BLOCKED;
            map[0][i] = BLOCKED;
        }
        for (int j = 0; j < height; j++) {
            map[j][width-1] = BLOCKED;
            map[j][0] = BLOCKED;
        }
    }
}
