package com.example.alexander.roguelikeandroid.input;


import android.view.View;

import com.example.alexander.roguelikeandroid.R;

import static com.example.alexander.roguelikeandroid.R.id.buttonDown;
import static com.example.alexander.roguelikeandroid.R.id.buttonLeft;
import static com.example.alexander.roguelikeandroid.R.id.buttonRight;

/**
 * Created by Alexander on 10.11.2016.
 */


public class GameInput {

    public static final int DEFAULT_KEY = -1;
    public static final int KEY_ESC = 0;
    public static final int KEY_UP = 1;
    public static final int KEY_DOWN = 2;
    public static final int KEY_LEFT = 3;
    public static final int KEY_RIGHT = 4;


    private InputListener inputListener;


    public GameInput() {
    }

    private int handleMotionEvent() {

/*        int pX = (int) (gameHandler.getPlayer().getX() - motionEvent.getX());
        int pY = (int) (gameHandler.getPlayer().getY() - motionEvent.getY());

        if (motionEvent.getX() >= pX && motionEvent.getY() <= pY) {
            if (pX >= pY) {
                return KEY_RIGHT;
            } else {
                return KEY_UP;
            }
        } else if (motionEvent.getX() <= pX && motionEvent.getY() >= pY) {
            if (pX >= pY) {
                return KEY_LEFT;
            } else {
                return KEY_DOWN;
            }
        } else if (motionEvent.getX() >= pX && motionEvent.getY() >= pY) {
            if (pX >= pY) {
                return KEY_RIGHT;
            } else {
                return KEY_DOWN;
            }
        } else if (motionEvent.getX() >= pX && motionEvent.getY() <= pY) {
            if (pX >= pY) {
                return KEY_LEFT;
            } else {
                return KEY_UP;
            }
        }*/
        return DEFAULT_KEY;
    }

    public View.OnClickListener clickButton = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.buttonUp:
                    inputListener.OnInput(KEY_UP);
                    break;
                case buttonDown:
                    inputListener.OnInput(KEY_DOWN);
                    break;
                case buttonLeft:
                    inputListener.OnInput(KEY_LEFT);
                    break;
                case buttonRight:
                    inputListener.OnInput(KEY_RIGHT);
                    break;
                default:
                    inputListener.OnInput(DEFAULT_KEY);
                    break;
            }
        }
    };

    public void setListener(InputListener listener) {
        this.inputListener = listener;
    }
}
