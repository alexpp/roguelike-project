package com.example.alexander.roguelikeandroid.game;


import android.graphics.Color;

import com.example.alexander.roguelikeandroid.game.map.Map;
import com.example.alexander.roguelikeandroid.game.map.Tile;
import com.example.alexander.roguelikeandroid.objects.BaseObject;
import com.example.alexander.roguelikeandroid.objects.monsters.Monster;
import com.example.alexander.roguelikeandroid.objects.player.Player;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Alexander on 11.11.2016.
 */

public class GameInitializer {

    private static Map map;
    private static final int scale = 32;
    private static int N = 50;


    public static ArrayList<BaseObject> init(int width, int height) {
        ArrayList<BaseObject> objects = new ArrayList<>();
        Player player = new Player(2*scale, 2*scale, '@', BaseObject.PLAYER, Color.WHITE, 100, 100, 100);
        objects.add(player);
        map = new Map(N, N);
        map.clear();
        map.wallPerimeter();
        int k = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                switch (map.map[i][j]) {
                    case Map.FREE:
                        objects.add(new Tile(j*scale, i*scale, '.', false));
                        break;
                    case Map.BLOCKED:
                        objects.add(new Tile(j*scale, i*scale, '#', true));
                        break;
                }
                k++;
            }
        }
        initActors(objects, width, height);
        return objects;
    }

    private static void initActors(ArrayList<BaseObject> objects, int width, int height) {
        for (int i = 0; i < 15; i++) {
            int x = new Random().nextInt(N);
            int y = new Random().nextInt(N);
            if (map.map[y][x] == Map.FREE) {
                objects.add(new Monster(x*scale, y*scale, 'b', "bat", Color.YELLOW, 10, 2, 2));
            }
        }
    }
/*    public void aiMonster(Monster monster, Player player, Point point){

        int x = new Random().nextInt(79);
        int y = new Random().nextInt(24);
        int dx = player.x - monster.x;
        int dy = player.y - monster.y;
        if (Math.abs(dx) + Math.abs(dy) > 6){
            while (!collisionMonster(monster, point, map.map[x][y])){

            }
        }
    }*/

    public static Map getMap() {
        return map;
    }
}
