package com.example.alexander.roguelikeandroid.output;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.view.SurfaceHolder;

import com.example.alexander.roguelikeandroid.game.GameHandler;
import com.example.alexander.roguelikeandroid.objects.BaseObject;

import static java.lang.Math.abs;

class DrawThread extends Thread{
    private boolean runFlag = false;
    private SurfaceHolder surfaceHolder;
    private Bitmap picture;
    private Matrix matrix;
    private long prevTime;
    private GameHandler gameHandler;

    public DrawThread(SurfaceHolder surfaceHolder, Resources resources, GameHandler gameHandler){
        this.surfaceHolder = surfaceHolder;
        this.gameHandler = gameHandler;

        // загружаем картинку, которую будем отрисовывать
        //picture = BitmapFactory.decodeResource(resources, R.drawable.icon);

        // формируем матрицу преобразований для картинки
        matrix = new Matrix();
        matrix.postScale(3.0f, 3.0f);
        matrix.postTranslate(100.0f, 100.0f);

        // сохраняем текущее время
        prevTime = System.currentTimeMillis();
    }

    public void setRunning(boolean run) {
        runFlag = run;
    }

    @Override
    public void run() {
        Canvas canvas;
        while (runFlag) {
            // получаем текущее время и вычисляем разницу с предыдущим 
            // сохраненным моментом времени
            long now = System.currentTimeMillis();
            long elapsedTime = now - prevTime;
            if (elapsedTime > 30){
                // если прошло больше 30 миллисекунд - сохраним текущее время
                // и повернем картинку на 2 градуса.
                // точка вращения - центр картинки
                prevTime = now;
                //matrix.preRotate(2.0f, picture.getWidth() / 2, picture.getHeight() / 2);
            }
            canvas = null;
            try {
                // получаем объект Canvas и выполняем отрисовку
                canvas = surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    canvas.drawColor(Color.BLACK);
                    //canvas.drawText(String.valueOf("X"), 150, 150, p);
                    //surfaceHolder.unlockCanvasAndPost(canvas);
                    for (BaseObject baseObject : gameHandler.getObjects()) {
                        int x = abs(gameHandler.getPlayer().getX() - baseObject.getX());
                        int y = abs(gameHandler.getPlayer().getY() - baseObject.getY());
                        if(x < surfaceHolder.getSurfaceFrame().width()
                                && y < surfaceHolder.getSurfaceFrame().height()) {
                            canvas.drawText(String.valueOf(baseObject.getCharModel()), baseObject.getX() - gameHandler.mX, baseObject.getY() - gameHandler.mY, baseObject.paint);
                        }
                        /*if (baseObject.getX() < surfaceHolder.getSurfaceFrame().width() && baseObject.getY() < surfaceHolder.getSurfaceFrame().height()){
                        }*/
                    }
                    for (BaseObject baseObject : gameHandler.getMonster()) {
                        int x = abs(gameHandler.getPlayer().getX() - baseObject.getX());
                        int y = abs(gameHandler.getPlayer().getY() - baseObject.getY());
                        if(x < surfaceHolder.getSurfaceFrame().width()
                                && y < surfaceHolder.getSurfaceFrame().height()) {
                            canvas.drawText(String.valueOf(baseObject.getCharModel()), baseObject.getX() - gameHandler.mX, baseObject.getY() - gameHandler.mY, baseObject.paint);
                        }
                    }
                    //canvas.drawBitmap(picture, matrix, null);
                }
            } 
            finally {
                if (canvas != null) {
                    // отрисовка выполнена. выводим результат на экран
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}