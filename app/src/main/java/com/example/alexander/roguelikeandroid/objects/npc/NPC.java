package com.example.alexander.roguelikeandroid.objects.npc;


import com.example.alexander.roguelikeandroid.objects.BaseAliveObject;

/**
 * Created by Alexander on 10.11.2016.
 */

public class NPC extends BaseAliveObject {

    public NPC(int x, int y, char charModel, String name, int color, int health, int armor, int strength) {
        super(x, y, charModel, name, color, health, armor, strength);
    }
}
