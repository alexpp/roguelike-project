package com.example.alexander.roguelikeandroid.output;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.example.alexander.roguelikeandroid.objects.BaseObject;

import static android.graphics.Color.RED;


/**
 * Created by Alexander on 10.11.2016.
 */

public class GameOutput implements OutputDisplayInterface {

    private SurfaceView surfaceView;
    private SurfaceHolder mSurfaceHolder;

    public GameOutput(SurfaceView surfaceView) {
        this.surfaceView = surfaceView;
        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                mSurfaceHolder = surfaceHolder;
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
                mSurfaceHolder = surfaceHolder;
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                mSurfaceHolder = surfaceHolder;
            }
        });
        //this.bitmap = Bitmap.createBitmap();
//        this.canvas = surfaceView.getHolder().lockCanvas();
    }

    @Override
    public void display(BaseObject object) {
        Paint p = new Paint();
//        switch (object.get)
        if (mSurfaceHolder != null) {
            Canvas canvas = mSurfaceHolder.lockCanvas();
            if (canvas != null) {
                canvas.drawText(String.valueOf(object.getCharModel()), object.getX(), object.getY(), new Paint(RED));
                mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        }
//        surfaceView.draw(object.getX(), object.getY(), object.getCharModel(), object.getCsiColor());
//        csi.refresh();
    }

}
