package com.example.alexander.roguelikeandroid.game.map;

/**
 * Created by Alexander on 11.11.2016.
 */

public class Rect {

    public int x1;
    public int x2;
    public int y1;
    public int y2;

    public Rect(int x, int y, int w, int h) {
        this.x1 = x;
        this.y1 = y;
        this.x2 = x + w;
        this.y2 = y + h;
    }

    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public int getY1() {
        return y1;
    }

    public int getY2() {
        return y2;
    }

    public int getCenterX() {
        return (this.x1 + this.x2) / 2;
    }

    public int getCenterY() {
        return (this.y1 + this.y2) / 2;
    }

    //проверяем пересечения двух прямоугольников.
    public boolean intersect(Rect other) {
        return (this.x1 <= this.x2 && this.x2 >= other.x1 && this.y1 <= other.y2 && this.y2 >= other.y1);
    }
}
