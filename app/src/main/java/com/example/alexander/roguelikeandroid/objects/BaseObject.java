package com.example.alexander.roguelikeandroid.objects;


import android.graphics.Paint;

/**
 * Created by Alexander on 10.11.2016.
 */

public abstract class BaseObject implements IBaseObject {

    public static final String MONSTER = "bat";
    public static final String PLAYER = "player";
    public static final String FLOOR = "floor";
    public static final String WALL = "wall";
    public int x;
    public int y;
    public char charModel;
    public String name;
    public Paint paint;

    public BaseObject(int x, int y, char charModel, String name, int color) {
        this.x = x;
        this.y = y;
        this.charModel = charModel;
        this.name = name;
        this.paint = new Paint();
        paint.setColor(color);
        paint.setTextSize(32);
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public char getCharModel() {
        return charModel;
    }

/*    @Override
    public CSIColor getCsiColor() {
        return csiColor;
    }*/

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void move(int dx, int dy) {
        x += dx;
        y += dy;
    }
}
