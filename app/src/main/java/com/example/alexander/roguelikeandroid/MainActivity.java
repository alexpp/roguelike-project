package com.example.alexander.roguelikeandroid;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.example.alexander.roguelikeandroid.output.MySurfaceView;


public class MainActivity extends AppCompatActivity {

    private MySurfaceView mySurfaceView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mySurfaceView = (MySurfaceView) findViewById(R.id.sv);
        mySurfaceView.listener = new MySurfaceView.CallBack() {
            @Override
            public void onCreateComplete() {
                findViewById(R.id.buttonUp).setOnClickListener(mySurfaceView.gameHandler.gameInput.clickButton);
                findViewById(R.id.buttonDown).setOnClickListener(mySurfaceView.gameHandler.gameInput.clickButton);
                findViewById(R.id.buttonLeft).setOnClickListener(mySurfaceView.gameHandler.gameInput.clickButton);
                findViewById(R.id.buttonRight).setOnClickListener(mySurfaceView.gameHandler.gameInput.clickButton);
            }
        };
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //System.exit(0);


        //setContentView(new GameView(this));

    }

}
