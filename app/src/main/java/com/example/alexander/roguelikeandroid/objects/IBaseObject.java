package com.example.alexander.roguelikeandroid.objects;


/**
 * Created by Alexander on 11.11.2016.
 */

public interface IBaseObject {
    int getX();

    int getY();

    char getCharModel();

   // CSIColor getCsiColor();

    String getName();

    void setX(int x);

    void setY(int y);

    void move(int dx, int dy);
}
