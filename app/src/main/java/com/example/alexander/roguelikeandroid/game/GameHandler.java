package com.example.alexander.roguelikeandroid.game;

import com.example.alexander.roguelikeandroid.game.map.Map;
import com.example.alexander.roguelikeandroid.input.GameInput;
import com.example.alexander.roguelikeandroid.input.InputListener;
import com.example.alexander.roguelikeandroid.objects.BaseObject;
import com.example.alexander.roguelikeandroid.objects.player.Player;
import com.example.alexander.roguelikeandroid.output.MySurfaceView;

import java.util.ArrayList;

import static com.example.alexander.roguelikeandroid.input.GameInput.KEY_ESC;

/**
 * Created by Alexander on 11.11.2016.
 */

public class GameHandler implements IGameHandler {
    private boolean stop;
    public GameInput gameInput;
    private GameLogic gameLogic;
    private MySurfaceView mySurfaceView;
    public  int mX = 0;
    public  int mY = 0;

    private ArrayList<BaseObject> objects;
    private Player player;
    private ArrayList<BaseObject> monsters;
    private Map map;

    public GameHandler(MySurfaceView surfaceView) {
        this.gameInput = new GameInput();
        this.gameInput.setListener(inputListener);
        this.gameLogic = new GameLogic(this);
        this.objects = GameInitializer.init(surfaceView.width, surfaceView.height);
        this.monsters = new ArrayList<>();
        this.mySurfaceView = surfaceView;
        map = GameInitializer.getMap();
        for (BaseObject object : this.objects) {
            if (object.name.equals(BaseObject.PLAYER)) {
                player = (Player) object;
            } else if (object.name.equals(BaseObject.MONSTER)){
                monsters.add(object);
            }
        }
    }

    public Map getMap() {
        return map;
    }

    @Override
    public int getWidth() {
        return mySurfaceView.width;
    }

    @Override
    public int getHeight() {
        return mySurfaceView.height;
    }

    @Override
    public int getStep() {
        return 32;
    }

    @Override
    public void run() {
    }

    private InputListener inputListener = new InputListener() {
        @Override
        public void OnInput(int key) {
            if (key != KEY_ESC) {
                gameLogic.handleKey(key);
            } else {
                stop = true;
            }
        }
    };

    /*private void drawAllObjects() {
        for (BaseObject object : objects) {
            gameOutput.display(object);
        }
        gameOutput.display(player);
    }*/

    @Override
    public ArrayList<BaseObject> getObjects() {
        return objects;
    }

    @Override
    public BaseObject getPlayer() {
        return player;
    }

    public ArrayList<BaseObject> getMonster() {
        return monsters;
    }

/*    @Override
    public BaseObject getMonster() {
        return monster;
    }*/
}
