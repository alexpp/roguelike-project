package com.example.alexander.roguelikeandroid.game;


import com.example.alexander.roguelikeandroid.game.map.Map;
import com.example.alexander.roguelikeandroid.objects.BaseObject;

import java.util.ArrayList;

/**
 * Created by Alexander on 11.11.2016.
 */

public interface IGameHandler {
    void run();
    ArrayList<BaseObject> getObjects();
    BaseObject getPlayer();
    ArrayList<BaseObject> getMonster();
    Map getMap();
    int getWidth();
    int getHeight();
    int getStep();
}
