package com.example.alexander.roguelikeandroid.game;


import android.graphics.Point;

import com.example.alexander.roguelikeandroid.game.map.Map;
import com.example.alexander.roguelikeandroid.objects.BaseObject;

/**
 * Created by Alexander on 11.11.2016.
 */

public interface IGameLogic {
    void handleKey(int key);
    boolean collisionObject(BaseObject player, Point dir, Map map);
}
