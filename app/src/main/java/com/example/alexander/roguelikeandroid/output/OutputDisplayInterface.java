package com.example.alexander.roguelikeandroid.output;

import com.example.alexander.roguelikeandroid.objects.BaseObject;

/**
 * Created by Alexander on 10.11.2016.
 */

public interface OutputDisplayInterface {
    void display(BaseObject object);
}
