package com.example.alexander.roguelikeandroid.input;

/**
 * Created by Alexander on 10.11.2016.
 */

public interface InputHandleInterface {
    int getInKey();
}
