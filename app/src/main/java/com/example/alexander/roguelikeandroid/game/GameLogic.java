package com.example.alexander.roguelikeandroid.game;


import android.graphics.Point;

import com.example.alexander.roguelikeandroid.game.map.Map;
import com.example.alexander.roguelikeandroid.objects.BaseObject;

import java.util.Random;

import static com.example.alexander.roguelikeandroid.input.GameInput.KEY_DOWN;
import static com.example.alexander.roguelikeandroid.input.GameInput.KEY_LEFT;
import static com.example.alexander.roguelikeandroid.input.GameInput.KEY_RIGHT;
import static com.example.alexander.roguelikeandroid.input.GameInput.KEY_UP;

/**
 * Created by Alexander on 10.11.2016.
 */

public class GameLogic implements IGameLogic {

    IGameHandler gameHandler;

    public GameLogic(IGameHandler gameHandler) {
        this.gameHandler = gameHandler;
    }

    @Override
    public void handleKey(int key) {
        if (key == KEY_UP && (gameHandler.getPlayer().getY() - gameHandler.getStep() >= 0)) {
            if (collisionObject(gameHandler.getPlayer(), new Point(0, -gameHandler.getStep()), gameHandler.getMap())) {
                gameHandler.getPlayer().move(0, -gameHandler.getStep());
                ((GameHandler)gameHandler).mY -=32;
            }
        }
        if (key == KEY_DOWN && (gameHandler.getPlayer().getY() + gameHandler.getStep() < gameHandler.getMap().height*gameHandler.getStep())) {
            if (collisionObject(gameHandler.getPlayer(), new Point(0, gameHandler.getStep()), gameHandler.getMap())) {
                gameHandler.getPlayer().move(0, gameHandler.getStep());
                ((GameHandler)gameHandler).mY +=32;
            }
        }
        if (key == KEY_LEFT && (gameHandler.getPlayer().getX() - gameHandler.getStep() >= 0)) {
            if (collisionObject(gameHandler.getPlayer(), new Point(-gameHandler.getStep(), 0), gameHandler.getMap())) {
                gameHandler.getPlayer().move(-gameHandler.getStep(), 0);
                ((GameHandler)gameHandler).mX -=32;
            }

        }
        if (key == KEY_RIGHT && (gameHandler.getPlayer().getX() + gameHandler.getStep() < gameHandler.getMap().width*gameHandler.getStep())) {
            if (collisionObject(gameHandler.getPlayer(), new Point(gameHandler.getStep(), 0), gameHandler.getMap())) {
                gameHandler.getPlayer().move(gameHandler.getStep(), 0);
                ((GameHandler)gameHandler).mX +=32;
            }
        }

        moveTo();
    }

    public void moveTo() {

        for (BaseObject object : gameHandler.getMonster()) {
            int dx = (new Random().nextInt(3)-1)*gameHandler.getStep();
            int dy= (new Random().nextInt(3)-1)*gameHandler.getStep();
            if (collisionObject(object, new Point(dx, dy), gameHandler.getMap())) {
                object.move(dx, dy);
            }
        }
    }

    public boolean collisionObject(BaseObject object, Point dir, Map map) {
        return map.map[(object.y + dir.y)/gameHandler.getStep()][(object.x + dir.x)/gameHandler.getStep()] == Map.FREE;
    }
}
