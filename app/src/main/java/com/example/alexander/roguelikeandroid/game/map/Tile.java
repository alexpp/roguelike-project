package com.example.alexander.roguelikeandroid.game.map;


import android.graphics.Color;

import com.example.alexander.roguelikeandroid.objects.BaseObject;

/**
 * Created by Alexander on 11.11.2016.
 */

public class Tile extends BaseObject {
    private boolean blocked;
    private boolean wall;

    public boolean isBlocked() {
        return blocked;
    }

    public boolean isWall() {
        return wall;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    public void setWall(boolean wall) {
        this.wall = wall;
    }

    public Tile(int x, int y, char c, boolean blocked) {
        super(x, y, c, blocked ? BaseObject.WALL : BaseObject.FLOOR, blocked ? Color.RED : Color.GRAY);
        this.blocked = blocked;
        this.wall = false;
    }

/*    public Tile(boolean blocked, boolean blockedSight) {
        this.blocked = blocked;
        this.wall = false;

    }*/
}
