package com.example.alexander.roguelikeandroid.input;

/**
 * Created by Alexander on 08.12.2016.
 */

public interface InputListener {

    void OnInput(int key);
}
